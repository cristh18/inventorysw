package com.nsdlp.co.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.nsdlp.co.data.DeliveryPointRepository;
import com.nsdlp.co.model.DeliveryPoint;

@FacesConverter(value = "deliveryPointConverter")
public class DeliveryPointConverter implements Converter{
	
	@Inject
    private DeliveryPointRepository deliveryPointRepository;
 
    @Override
    public DeliveryPoint getAsObject(FacesContext context, UIComponent component,
            String value) {
 
        if (value == null || value.isEmpty()) {
            return null;
        }
 
        try {
            DeliveryPoint obj = deliveryPointRepository
                    .findById(Long.valueOf(value));
             
            System.out.println(obj.getId());
             
            return obj;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConverterException(new FacesMessage(String.format(
                    "Cannot convert %s to DeliveryPoint", value)), e);
        }
    }
 
    @Override
    public String getAsString(FacesContext context, UIComponent component,
            Object value) {
 
        if (!(value instanceof DeliveryPoint)) {
            return null;
        }
 
        String s = String.valueOf(((DeliveryPoint) value).getId());
 
        return s;
    }


}
