package com.nsdlp.co.converter;

import java.util.Collection;

import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.primefaces.component.selectonemenu.SelectOneMenu;

import com.nsdlp.co.model.Client;

@FacesConverter(value = "clientConverter")
public class ClientConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if (component instanceof SelectOneMenu) {
			for (UIComponent child : component.getChildren()) {
				if (child instanceof UISelectItems) {
					UISelectItems selectItems = (UISelectItems) child;
					for (Client selectItemValue : (Collection<Client>) selectItems
							.getValue()) {
						if (selectItemValue instanceof Client) {
							String id = "" + selectItemValue.getId();
							if (value.equals(id)) {
								return selectItemValue;
							}
						}
					}
				}
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		String str = "";
		if (value instanceof Client) {
			str = "" + ((Client) value).getId();
		}
		return str;
	}

}