package com.nsdlp.co.constants;

public final class InventoryConstants {
	
	/**
	 * Bottle item name
	 */
	public static final String ITEM_NAME_BOTTLE = "Botellon";
	
	/**
	 * Basket item name
	 */
	public static final String ITEM_NAME_BASKET = "Canasta";

	/**
	 * Bottle price per unit
	 */
	public static final int PRICE_BOTTLE = 5000;
	
	/**
	 * Basket price per unit
	 */
	public static final int PRICE_BASKET = 4500;

}
