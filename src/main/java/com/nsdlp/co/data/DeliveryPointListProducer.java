package com.nsdlp.co.data;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import com.nsdlp.co.model.DeliveryPoint;

@RequestScoped
public class DeliveryPointListProducer {
	
	@Inject
	private DeliveryPointRepository deliveryPointRepository;

	/**
	 * 
	 */
	private List<DeliveryPoint> deliveryPointList;

	/**
	 * @return the clients
	 */
	@Produces
	@Named
	public List<DeliveryPoint> getDeliveryPointList() {
		return deliveryPointList;
	}

	public void onDeliveryPointListChanged(
			@Observes(notifyObserver = Reception.IF_EXISTS) final DeliveryPoint deliveryPoint) {
		retrieveAllDeliveryPointOrderedByName();
	}

	@PostConstruct
	public void retrieveAllDeliveryPointOrderedByName() {
		deliveryPointList = deliveryPointRepository.findAllOrderedByName();
	}

}
