package com.nsdlp.co.data;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.nsdlp.co.model.DeliveryPoint;
import com.nsdlp.co.model.Order;
import com.nsdlp.co.model.OrderDetail;

@Stateless
public class OrderDetailRepository {
	
	@Inject
	private EntityManager em;
	
	
	/**
	 * Get all DetailOrders from name
	 * 
	 * @return
	 */
	public List<OrderDetail> findAllOrderedByName() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<OrderDetail> criteria = cb.createQuery(OrderDetail.class);
		Root<OrderDetail> orderDetail = criteria.from(OrderDetail.class);
		
		criteria.select(orderDetail).orderBy(cb.asc(orderDetail.get("id")));
		return em.createQuery(criteria).getResultList();
	}
	
	
	/**
	 * Get all DetailOrders from order
	 * 
	 * @return
	 */
	public List<OrderDetail> findAllOrderDetailsByOrder(Order order) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<OrderDetail> criteria = cb.createQuery(OrderDetail.class);
		Root<OrderDetail> orderDetail = criteria.from(OrderDetail.class);
		
		Fetch<OrderDetail, Order> orderTemp = orderDetail.fetch("order", JoinType.LEFT);
		Fetch<Order, DeliveryPoint> deliveryPointTemp = orderTemp.fetch("deliveryPoint", JoinType.LEFT);
		deliveryPointTemp.fetch("client", JoinType.LEFT);

		Predicate condiciones = cb.equal(orderDetail.get("order"), order);

		criteria.select(orderDetail).where(condiciones);

		return em.createQuery(criteria).getResultList();
	}
	

}
