package com.nsdlp.co.data;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.nsdlp.co.model.Client;

@Stateless
public class ClientRepository {
	
	@Inject
    private EntityManager em;

	/**
	 * Get all clients
	 * @return
	 */
	public List<Client> findAllOrderedByName() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Client> criteria = cb.createQuery(Client.class);
        Root<Client> client = criteria.from(Client.class);       
        criteria.select(client).orderBy(cb.asc(client.get("id")));
        return em.createQuery(criteria).getResultList();
    }

}
