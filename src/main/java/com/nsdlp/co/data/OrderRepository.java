package com.nsdlp.co.data;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.nsdlp.co.model.Client;
import com.nsdlp.co.model.DeliveryPoint;
import com.nsdlp.co.model.Order;

@Stateless
public class OrderRepository {

	@Inject
	private EntityManager em;

	/**
	 * Get all items from order
	 * 
	 * @return
	 */
	public List<Order> findAllOrderedByName() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Order> criteria = cb.createQuery(Order.class);
		Root<Order> order = criteria.from(Order.class);
		
		Fetch<Order, DeliveryPoint> deliveryPointTemp = order.fetch("deliveryPoint", JoinType.LEFT);
		deliveryPointTemp.fetch("client", JoinType.LEFT);
		
		criteria.select(order).orderBy(cb.asc(order.get("id")));
		return em.createQuery(criteria).getResultList();
	}
	
	/**
	 * 
	 * @param startDate
	 * @param finalDate
	 * @param client
	 * @param deliveryPoint
	 * @return
	 */
	public List<Order> findOrdersByFilters(Date startDate, Date finalDate, Client client, DeliveryPoint deliveryPoint) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Order> criteria = cb.createQuery(Order.class);
		Root<Order> order = criteria.from(Order.class);
		
		Fetch<Order, DeliveryPoint> deliveryPointTemp = order.fetch("deliveryPoint", JoinType.LEFT);
		deliveryPointTemp.fetch("client", JoinType.LEFT);
		
		Expression<Date> orderDate = order.get("orderDate");
		Predicate validateOrderDate = cb.between(orderDate, startDate,
				finalDate);
		
		Predicate condiciones = null;
		
		if (client != null && deliveryPoint == null) {
			Predicate validateClient = cb.equal(order.get("deliveryPoint").get("client"),
					client);
			condiciones = cb.and(validateOrderDate, validateClient);
		}
		else if (client != null && deliveryPoint != null) {
			Predicate validateDeliveryPoint = cb.equal(order.get("deliveryPoint"),
					deliveryPoint);						
			condiciones = cb.and(validateOrderDate, validateDeliveryPoint);
//			condiciones = validateDeliveryPoint;
		}
		
		criteria.select(order).where(condiciones);
		
		try {
			return em.createQuery(criteria).getResultList();
		} catch (NoResultException nre) {
			nre.printStackTrace();
		} catch (NonUniqueResultException nure) {
			nure.printStackTrace();
		}
		return null;
	}
	
}
