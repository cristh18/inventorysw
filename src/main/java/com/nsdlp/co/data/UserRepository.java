package com.nsdlp.co.data;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.nsdlp.co.model.User;

@Stateless
public class UserRepository {
	
	@Inject
	private EntityManager em;

	/**
	 * Get all users 
	 * 
	 * @return
	 */
	public List<User> findAllOrderedByName() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<User> criteria = cb.createQuery(User.class);
		Root<User> user = criteria.from(User.class);
		criteria.select(user).orderBy(cb.asc(user.get("id")));
		return em.createQuery(criteria).getResultList();
	}
	
	/**
	 * 
	 * @param userData
	 * @return
	 */
	public User findUser(String userName, String userPassword) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<User> criteria = cb.createQuery(User.class);
		Root<User> user = criteria.from(User.class);
		
		Predicate validateUseName = cb.equal(user.get("userName"),
				userName);	
		Predicate validateUserPassword = cb.equal(user.get("userPassword"),
				userPassword);
		Predicate validateUserData = cb.and(validateUseName, validateUserPassword);
		
		criteria.select(user).where(validateUserData);
		
		
		try {
			return em.createQuery(criteria).getSingleResult();
		} catch (NoResultException nre) {
			nre.printStackTrace();
		} catch (NonUniqueResultException nure) {
			nure.printStackTrace();
		}
		return null;
	}

}
