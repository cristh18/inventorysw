package com.nsdlp.co.data;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.nsdlp.co.model.Inventory;


@Stateless
public class InventoryRepository {
	
	@Inject
    private EntityManager em;

	/**
	 * Get all items from inventory
	 * @return
	 */
	public List<Inventory> findAllOrderedByName() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Inventory> criteria = cb.createQuery(Inventory.class);
        Root<Inventory> inventory = criteria.from(Inventory.class);       
        criteria.select(inventory).orderBy(cb.asc(inventory.get("name")));
        return em.createQuery(criteria).getResultList();
    }

}
