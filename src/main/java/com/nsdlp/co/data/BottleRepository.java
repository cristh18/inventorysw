package com.nsdlp.co.data;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.nsdlp.co.model.Bottle;

@Stateless
public class BottleRepository {

	@Inject
	private EntityManager em;

	/**
	 * Find blottle by id
	 * @param id
	 * @return
	 */
	public Bottle findById(Long id) {
		return em.find(Bottle.class, id);
	}

	/**
	 * Get bottle
	 * 
	 * @return
	 */
	public Bottle findBottleByName() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Bottle> criteria = cb.createQuery(Bottle.class);
		Root<Bottle> bottle = criteria.from(Bottle.class);
		criteria.select(bottle).orderBy(cb.asc(bottle.get("name")));
		return em.createQuery(criteria).getSingleResult();
	}

}
