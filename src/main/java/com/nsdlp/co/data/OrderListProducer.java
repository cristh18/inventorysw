package com.nsdlp.co.data;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import com.nsdlp.co.model.Order;

@RequestScoped
public class OrderListProducer {

	@Inject
	private OrderRepository orderRepository;

	/**
	 * 
	 */
	private List<Order> orderList;

	/**
	 * @return the clients
	 */
	@Produces
	@Named
	public List<Order> getOrderList() {
		return orderList;
	}

	public void onOrderListChanged(
			@Observes(notifyObserver = Reception.IF_EXISTS) final Order order) {
		retrieveAllOrderListOrderedByName();
	}

	@PostConstruct
	public void retrieveAllOrderListOrderedByName() {
		orderList = orderRepository.findAllOrderedByName();
	}

}
