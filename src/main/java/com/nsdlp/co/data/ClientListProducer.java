package com.nsdlp.co.data;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import com.nsdlp.co.model.Client;

/**
 * 
 * @author Cristhian
 *
 */
@RequestScoped
public class ClientListProducer {

	@Inject
	private ClientRepository clientRepository;

	/**
	 * 
	 */
	private List<Client> clientList;

	/**
	 * @return the clients
	 */
	@Produces
	@Named
	public List<Client> getClientList() {
		return clientList;
	}

	public void onClientListChanged(
			@Observes(notifyObserver = Reception.IF_EXISTS) final Client client) {
		retrieveAllClientListOrderedByName();
	}

	@PostConstruct
	public void retrieveAllClientListOrderedByName() {
		clientList = clientRepository.findAllOrderedByName();
	}

}
