package com.nsdlp.co.data;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import com.nsdlp.co.model.Inventory;

@RequestScoped
public class InventoryListProducer {

	@Inject
	private InventoryRepository inventoryRepository;

	/**
	 * 
	 */
	private List<Inventory> inventoryList;

	/**
	 * @return the clients
	 */
	@Produces
	@Named
	public List<Inventory> getInventoryList() {
		return inventoryList;
	}

	public void oninventoryListChanged(
			@Observes(notifyObserver = Reception.IF_EXISTS) final Inventory inventory) {
		retrieveAllInventoryItemsOrderedByName();
	}

	@PostConstruct
	public void retrieveAllInventoryItemsOrderedByName() {
		inventoryList = inventoryRepository.findAllOrderedByName();
	}

}
