package com.nsdlp.co.data;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.nsdlp.co.model.Client;
import com.nsdlp.co.model.DeliveryPoint;

@Stateless
public class DeliveryPointRepository {
	
	@Inject
    private EntityManager em;

	 /**
     * 
     * @param id
     * @return
     */
    public DeliveryPoint findById(Long id) {
        return em.find(DeliveryPoint.class, id);
    }
	
	/**
	 * Get all delivery points
	 * @return
	 */
	public List<DeliveryPoint> findAllOrderedByName() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<DeliveryPoint> criteria = cb.createQuery(DeliveryPoint.class);
        Root<DeliveryPoint> deliveryPoint = criteria.from(DeliveryPoint.class);       
        criteria.select(deliveryPoint).orderBy(cb.asc(deliveryPoint.get("name")));
        return em.createQuery(criteria).getResultList();
    }
	
	
	/**
	 * Get all delivery points by client
	 * @return
	 */
	public List<DeliveryPoint> findAllOrderedByNameByClientId(Client client) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<DeliveryPoint> criteria = cb.createQuery(DeliveryPoint.class);
        Root<DeliveryPoint> deliveryPoint = criteria.from(DeliveryPoint.class);
        
//        Join valorado = deliveryPoint
//				.join("client", JoinType.LEFT);
        
        deliveryPoint.fetch("client", JoinType.LEFT);
        
        Predicate deliveryPoinyByClient = cb.equal(
				deliveryPoint.get("client"), client);
        
        
        criteria.select(deliveryPoint).where(deliveryPoinyByClient);
        return em.createQuery(criteria).getResultList();
    }

}
