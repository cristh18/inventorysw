package com.nsdlp.co.data;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.nsdlp.co.model.Basket;
@Stateless
public class BasketRepository {

	@Inject
    private EntityManager em;
	
	/**
	 * Find basket by id
	 * @param id
	 * @return
	 */
	public Basket findById(Long id) {
		return em.find(Basket.class, id);
	}

	/**
	 * Get basket
	 * @return
	 */
	public Basket findBasketByName() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Basket> criteria = cb.createQuery(Basket.class);
        Root<Basket> basket = criteria.from(Basket.class);       
        criteria.select(basket).orderBy(cb.asc(basket.get("name")));
        return em.createQuery(criteria).getSingleResult();
    }
	
}
