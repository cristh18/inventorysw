package com.nsdlp.co.model;

import com.nsdlp.co.model.Client;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * 
 * @author Cristhian Leonardo Tolosa
 */
@Entity
public class User implements Serializable, Cloneable {
	/** @generated */
	private static final long serialVersionUID = 1L;

	/** @generated */
	public User() {
	}

	/**
	 * ------------------------------------------
	 * The primary key.
	 *
	 * @generated
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	/** @generated */
	public void setId(final Long id) {
		this.id = id;
	}

	/** @generated */
	private Long id = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@Column(length = 20, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public String getUserName() {
		return userName;
	}

	/** @generated */
	public void setUserName(final String userName) {
		this.userName = userName;
	}

	/** @generated */
	private String userName = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@Column(length = 30, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public String getUserPassword() {
		return userPassword;
	}

	/** @generated */
	public void setUserPassword(final String userPassword) {
		this.userPassword = userPassword;
	}

	/** @generated */
	private String userPassword = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.REFRESH)
	public List<Client> getClients() {
		if (this.clients == null) {
			this.clients = new ArrayList<Client>();
		}
		return clients;
	}

	/** @generated */
	public void setClients(final List<Client> clients) {
		this.clients = clients;
	}

	/**
	 * Associate User with Client
	 * @generated
	 */
	public void addClient(Client client) {
		if (client == null) {
			return;
		}
		getClients().add(client);
		client.setUser(this);
	}

	/**
	 * Unassociate User from Client
	 * @generated
	 */
	public void removeClient(Client client) {
		if (client == null) {
			return;
		}
		getClients().remove(client);
		client.setUser(null);
	}

	/**
	 * @generated
	 */
	public void removeAllClients() {
		List<Client> remove = new ArrayList<Client>();
		remove.addAll(getClients());
		for (Client element : remove) {
			removeClient(element);
		}
	}

	/** @generated */
	private List<Client> clients = null;

	// ------------------------------------------
	// Utils
	// ------------------------------------------



	/** @generated */
	public User deepClone() throws Exception {
		User clone = (User) super.clone();
		clone.setId(null);

		clone.setClients(null);
		for (Client kid : this.getClients()) {
			clone.addClient(kid.deepClone());
		}
		return clone;
	}
	
	/** @generated */
	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ((id == null) ? super.hashCode() : id.hashCode());
		return result;
	}

	/** @generated */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof User))
			return false;
		final User other = (User) obj;
		if (id == null) {
			if (other.getId() != null)
				return false;
		} else if (!id.equals(other.getId()))
			return false;
		return true;
	}	
}
