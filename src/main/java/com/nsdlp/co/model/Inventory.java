package com.nsdlp.co.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 *
 * @author Cristhian Leonardo Tolosa
 */
@Entity
@DiscriminatorColumn(name = "ITEM", length = 20, discriminatorType = DiscriminatorType.STRING) 
@DiscriminatorValue(value = "INVENTORY")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Inventory implements Serializable, Cloneable {
	/** @generated */
	private static final long serialVersionUID = 1L;

	/** @generated */
	public Inventory() {
	}

	/**
	 * ------------------------------------------
	 * The primary key.
	 *
	 * @generated
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	/** @generated */
	public void setId(final Long id) {
		this.id = id;
	}

	/** @generated */
	private Long id = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@Column(length = 20, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public String getName() {
		return name;
	}

	/** @generated */
	public void setName(final String name) {
		this.name = name;
	}

	/** @generated */
	private String name = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@Column(length = 0, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public Long getQuantity() {
		return quantity;
	}

	/** @generated */
	public void setQuantity(final Long quantity) {
		this.quantity = quantity;
	}

	/** @generated */
	private Long quantity = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@Column(length = 0, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public Double getValue() {
		return value;
	}

	/** @generated */
	public void setValue(final Double value) {
		this.value = value;
	}

	/** @generated */
	private Double value = null;

	// ------------------------------------------
	// Utils
	// ------------------------------------------



	/** @generated */
	public Inventory deepClone() throws Exception {
		Inventory clone = (Inventory) super.clone();
		clone.setId(null);
		return clone;
	}
	
	/** @generated */
	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ((id == null) ? super.hashCode() : id.hashCode());
		return result;
	}

	/** @generated */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Inventory))
			return false;
		final Inventory other = (Inventory) obj;
		if (id == null) {
			if (other.getId() != null)
				return false;
		} else if (!id.equals(other.getId()))
			return false;
		return true;
	}	
	
	/* (non-Javadoc)
	* @see java.lang.Object#toString()
	*/
	public String toString(){
	return new StringBuilder().append("Inventory [ID=").append(id).append("][NAM=").append(name).append("]").toString();
	}
}
