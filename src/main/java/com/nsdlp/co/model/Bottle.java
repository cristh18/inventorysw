package com.nsdlp.co.model;

import com.nsdlp.co.model.Inventory;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

/**
 *
 * @author Cristhian Leonardo Tolosa
 */
@Entity
@DiscriminatorValue(value = "ITEM_BOTTLE")
public class Bottle extends Inventory {
	/** @generated */
	private static final long serialVersionUID = 1L;

	/** @generated */
	public Bottle() {
	}

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "bottle", cascade = CascadeType.REFRESH)
	public List<Order> getOrders() {
		if (this.orders == null) {
			this.orders = new ArrayList<Order>();
		}
		return orders;
	}

	/** @generated */
	public void setOrders(final List<Order> orders) {
		this.orders = orders;
	}

	/**
	 * Associate Bottle with Order
	 * @generated
	 */
	public void addOrder(Order order) {
		if (order == null) {
			return;
		}
		getOrders().add(order);
		order.setBottle(this);
	}

	/**
	 * Unassociate Bottle from Order
	 * @generated
	 */
	public void removeOrder(Order order) {
		if (order == null) {
			return;
		}
		getOrders().remove(order);
		order.setBottle(null);
	}

	/**
	 * @generated
	 */
	public void removeAllOrders() {
		List<Order> remove = new ArrayList<Order>();
		remove.addAll(getOrders());
		for (Order element : remove) {
			removeOrder(element);
		}
	}

	/** @generated */
	private List<Order> orders = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "bottle", cascade = CascadeType.REFRESH)
	public List<Reception> getReceptions() {
		if (this.receptions == null) {
			this.receptions = new ArrayList<Reception>();
		}
		return receptions;
	}

	/** @generated */
	public void setReceptions(final List<Reception> receptions) {
		this.receptions = receptions;
	}

	/**
	 * Associate Bottle with Reception
	 * @generated
	 */
	public void addReception(Reception reception) {
		if (reception == null) {
			return;
		}
		getReceptions().add(reception);
		reception.setBottle(this);
	}

	/**
	 * Unassociate Bottle from Reception
	 * @generated
	 */
	public void removeReception(Reception reception) {
		if (reception == null) {
			return;
		}
		getReceptions().remove(reception);
		reception.setBottle(null);
	}

	/**
	 * @generated
	 */
	public void removeAllReceptions() {
		List<Reception> remove = new ArrayList<Reception>();
		remove.addAll(getReceptions());
		for (Reception element : remove) {
			removeReception(element);
		}
	}

	/** @generated */
	private List<Reception> receptions = null;

	// ------------------------------------------
	// Utils
	// ------------------------------------------



	/** @generated */
	public Bottle deepClone() throws Exception {
		Bottle clone = (Bottle) super.deepClone();

		clone.setOrders(null);
		for (Order kid : this.getOrders()) {
			clone.addOrder(kid.deepClone());
		}

		clone.setReceptions(null);
		for (Reception kid : this.getReceptions()) {
			clone.addReception(kid.deepClone());
		}
		return clone;
	}
	
	/* (non-Javadoc)
	* @see java.lang.Object#toString()
	*/
	public String toString(){
	return new StringBuilder().append("Bottle [ID=").append(super.getId()).append("][NAM=").append(super.getName()).append("]").toString();
	}
	
}
