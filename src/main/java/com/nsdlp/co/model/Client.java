package com.nsdlp.co.model;

import com.nsdlp.co.model.DeliveryPoint;
import com.nsdlp.co.model.User;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author Cristhian Leonardo Tolosa
 */
@Entity
public class Client implements Serializable, Cloneable {
	/** @generated */
	private static final long serialVersionUID = 1L;

	/** @generated */
	public Client() {
	}

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "client", cascade = CascadeType.REFRESH)
	public List<DeliveryPoint> getDeliveryPoints() {
		if (this.deliveryPoints == null) {
			this.deliveryPoints = new ArrayList<DeliveryPoint>();
		}
		return deliveryPoints;
	}

	/** @generated */
	public void setDeliveryPoints(final List<DeliveryPoint> deliveryPoints) {
		this.deliveryPoints = deliveryPoints;
	}

	/**
	 * Associate Client with DeliveryPoint
	 * @generated
	 */
	public void addDeliveryPoint(DeliveryPoint deliveryPoint) {
		if (deliveryPoint == null) {
			return;
		}
		getDeliveryPoints().add(deliveryPoint);
		deliveryPoint.setClient(this);
	}

	/**
	 * Unassociate Client from DeliveryPoint
	 * @generated
	 */
	public void removeDeliveryPoint(DeliveryPoint deliveryPoint) {
		if (deliveryPoint == null) {
			return;
		}
		getDeliveryPoints().remove(deliveryPoint);
		deliveryPoint.setClient(null);
	}

	/**
	 * @generated
	 */
	public void removeAllDeliveryPoints() {
		List<DeliveryPoint> remove = new ArrayList<DeliveryPoint>();
		remove.addAll(getDeliveryPoints());
		for (DeliveryPoint element : remove) {
			removeDeliveryPoint(element);
		}
	}

	/** @generated */
	private List<DeliveryPoint> deliveryPoints = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@Column(length = 20, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public String getName() {
		return name;
	}

	/** @generated */
	public void setName(final String name) {
		this.name = name;
	}

	/** @generated */
	private String name = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@Column(length = 25, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public String getLastName() {
		return lastName;
	}

	/** @generated */
	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	/** @generated */
	private String lastName = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@Column(length = 40, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public String getStreetAddress() {
		return streetAddress;
	}

	/** @generated */
	public void setStreetAddress(final String streetAddress) {
		this.streetAddress = streetAddress;
	}

	/** @generated */
	private String streetAddress = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@Column(length = 20, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/** @generated */
	public void setPhoneNumber(final String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/** @generated */
	private String phoneNumber = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	public User getUser() {
		return user;
	}

	/** @generated */
	public void setUser(final User user) {
		this.user = user;
	}

	/** @generated */
	private User user = null;

	/**
	 * ------------------------------------------
	 * The primary key.
	 *
	 * @generated
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	/** @generated */
	public void setId(final Long id) {
		this.id = id;
	}

	/** @generated */
	private Long id = null;

	// ------------------------------------------
	// Utils
	// ------------------------------------------



	/** @generated */
	public Client deepClone() throws Exception {
		Client clone = (Client) super.clone();
		clone.setId(null);

		clone.setDeliveryPoints(null);
		for (DeliveryPoint kid : this.getDeliveryPoints()) {
			clone.addDeliveryPoint(kid.deepClone());
		}
		return clone;
	}
	
	/** @generated */
	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ((id == null) ? super.hashCode() : id.hashCode());
		return result;
	}

	/** @generated */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Client))
			return false;
		final Client other = (Client) obj;
		if (id == null) {
			if (other.getId() != null)
				return false;
		} else if (!id.equals(other.getId()))
			return false;
		return true;
	}	
}
