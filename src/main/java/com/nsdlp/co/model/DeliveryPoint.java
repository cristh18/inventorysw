package com.nsdlp.co.model;

import com.nsdlp.co.model.Client;
import com.nsdlp.co.model.Order;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author Cristhian Leonardo Tolosa
 */
@Entity
public class DeliveryPoint implements Serializable, Cloneable {
	/** @generated */
	private static final long serialVersionUID = 1L;

	/** @generated */
	public DeliveryPoint() {
	}

	/**
	 * ------------------------------------------
	 * The primary key.
	 *
	 * @generated
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	/** @generated */
	public void setId(final Long id) {
		this.id = id;
	}

	/** @generated */
	private Long id = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	public Client getClient() {
		return client;
	}

	/** @generated */
	public void setClient(final Client client) {
		this.client = client;
	}

	/** @generated */
	private Client client = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@Column(length = 25, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public String getName() {
		return name;
	}

	/** @generated */
	public void setName(final String name) {
		this.name = name;
	}

	/** @generated */
	private String name = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@Column(length = 30, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public String getAddress() {
		return address;
	}

	/** @generated */
	public void setAddress(final String address) {
		this.address = address;
	}

	/** @generated */
	private String address = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "deliveryPoint", cascade = CascadeType.REFRESH)
	public List<Order> getOrders() {
		if (this.orders == null) {
			this.orders = new ArrayList<Order>();
		}
		return orders;
	}

	/** @generated */
	public void setOrders(final List<Order> orders) {
		this.orders = orders;
	}

	/**
	 * Associate DeliveryPoint with Order
	 * @generated
	 */
	public void addOrder(Order order) {
		if (order == null) {
			return;
		}
		getOrders().add(order);
		order.setDeliveryPoint(this);
	}

	/**
	 * Unassociate DeliveryPoint from Order
	 * @generated
	 */
	public void removeOrder(Order order) {
		if (order == null) {
			return;
		}
		getOrders().remove(order);
		order.setDeliveryPoint(null);
	}

	/**
	 * @generated
	 */
	public void removeAllOrders() {
		List<Order> remove = new ArrayList<Order>();
		remove.addAll(getOrders());
		for (Order element : remove) {
			removeOrder(element);
		}
	}

	/** @generated */
	private List<Order> orders = null;

	// ------------------------------------------
	// Utils
	// ------------------------------------------



	/** @generated */
	public DeliveryPoint deepClone() throws Exception {
		DeliveryPoint clone = (DeliveryPoint) super.clone();
		clone.setId(null);

		clone.setOrders(null);
		for (Order kid : this.getOrders()) {
			clone.addOrder(kid.deepClone());
		}
		return clone;
	}
	
	/** @generated */
	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ((id == null) ? super.hashCode() : id.hashCode());
		return result;
	}

	/** @generated */
	@Override
	public boolean equals(Object object) {
		// Basic checks.
		if (object == this)
			return true;
		if (object == null || getClass() != object.getClass())
			return false;

		// Property checks.
		DeliveryPoint other = (DeliveryPoint) object;
		if (name == null ? other.name != null : !name.equals(other.name))
			return false;
		if (id != other.id)
			return false;

		// All passed.
		return true;
	}
}