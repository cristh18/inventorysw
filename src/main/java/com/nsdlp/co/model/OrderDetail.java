package com.nsdlp.co.model;

import com.nsdlp.co.model.Order;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 * 
 * @author Cristhian Leonardo Tolosa
 */
@Entity
public class OrderDetail implements Serializable, Cloneable {
	/** @generated */
	private static final long serialVersionUID = 1L;

	/** @generated */
	public OrderDetail() {
	}

	/**
	 * ------------------------------------------
	 * The primary key.
	 *
	 * @generated
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	/** @generated */
	public void setId(final Long id) {
		this.id = id;
	}

	/** @generated */
	private Long id = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@Column(length = 20, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public String getItemName() {
		return itemName;
	}

	/** @generated */
	public void setItemName(final String itemName) {
		this.itemName = itemName;
	}

	/** @generated */
	private String itemName = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@Column(length = 0, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public Long getItemId() {
		return itemId;
	}

	/** @generated */
	public void setItemId(final Long itemId) {
		this.itemId = itemId;
	}

	/** @generated */
	private Long itemId = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@Column(length = 0, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public Date getOrderDateDetail() {		
		return orderDateDetail;
	}

	/** @generated */
	public void setOrderDateDetail(final Date orderDateDetail) {
		this.orderDateDetail = orderDateDetail;
	}

	/** @generated */
	private Date orderDateDetail = new Date();

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@Column(length = 0, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public Long getTotal() {
		return total;
	}

	/** @generated */
	public void setTotal(final Long total) {
		this.total = total;
	}

	/** @generated */
	private Long total = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	public Order getOrder() {
		return order;
	}

	/** @generated */
	public void setOrder(final Order order) {
		this.order = order;
	}

	/** @generated */
	private Order order = null;
	
	/**
	 * 
	 * @return
	 */
	public Double getPrice() {
		return price;
	}
	
	/**
	 * 
	 * @param price
	 */
	public void setPrice(Double price) {
		this.price = price;
	}
	
	@Transient
	private Double price;

	// ------------------------------------------
	// Utils
	// ------------------------------------------



	/** @generated */
	public OrderDetail deepClone() throws Exception {
		OrderDetail clone = (OrderDetail) super.clone();
		clone.setId(null);
		return clone;
	}
	
	/** @generated */
	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ((id == null) ? super.hashCode() : id.hashCode());
		return result;
	}

	/** @generated */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof OrderDetail))
			return false;
		final OrderDetail other = (OrderDetail) obj;
		if (id == null) {
			if (other.getId() != null)
				return false;
		} else if (!id.equals(other.getId()))
			return false;
		return true;
	}	
}
