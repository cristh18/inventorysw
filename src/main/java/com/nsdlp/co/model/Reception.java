package com.nsdlp.co.model;

import com.nsdlp.co.model.Basket;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Cristhian Leonardo Tolosa
 */
@Entity
public class Reception implements Serializable, Cloneable {
	/** @generated */
	private static final long serialVersionUID = 1L;

	/** @generated */
	public Reception() {
	}

	/**
	 * ------------------------------------------
	 * The primary key.
	 *
	 * @generated
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	/** @generated */
	public void setId(final Long id) {
		this.id = id;
	}

	/** @generated */
	private Long id = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@Column(length = 0, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public Date getReceptionDate() {
		return receptionDate;
	}

	/** @generated */
	public void setReceptionDate(final Date receptionDate) {
		this.receptionDate = receptionDate;
	}

	/** @generated */
	private Date receptionDate = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@Column(length = 0, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public Long getTotal() {
		return total;
	}

	/** @generated */
	public void setTotal(final Long total) {
		this.total = total;
	}

	/** @generated */
	private Long total = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	public Bottle getBottle() {
		return bottle;
	}

	/** @generated */
	public void setBottle(final Bottle bottle) {
		this.bottle = bottle;
	}

	/** @generated */
	private Bottle bottle = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	public Basket getBasket() {
		return basket;
	}

	/** @generated */
	public void setBasket(final Basket basket) {
		this.basket = basket;
	}

	/** @generated */
	private Basket basket = null;

	// ------------------------------------------
	// Utils
	// ------------------------------------------



	/** @generated */
	public Reception deepClone() throws Exception {
		Reception clone = (Reception) super.clone();
		clone.setId(null);
		return clone;
	}
	
	/** @generated */
	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ((id == null) ? super.hashCode() : id.hashCode());
		return result;
	}

	/** @generated */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Reception))
			return false;
		final Reception other = (Reception) obj;
		if (id == null) {
			if (other.getId() != null)
				return false;
		} else if (!id.equals(other.getId()))
			return false;
		return true;
	}	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return new StringBuilder().append("Reception [ID=").append(id)
				.append("][RDA=").append(receptionDate).append("]").append("][TOT=")
				.append(total).append("]").toString();
	}
}
