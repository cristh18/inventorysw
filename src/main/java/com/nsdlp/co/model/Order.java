package com.nsdlp.co.model;

import com.nsdlp.co.model.Basket;
import com.nsdlp.co.model.Bottle;
import com.nsdlp.co.model.DeliveryPoint;
import com.nsdlp.co.model.OrderDetail;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author Cristhian Leonardo Tolosa
 */
@Entity
public class Order implements Serializable, Cloneable {
	/** @generated */
	private static final long serialVersionUID = 1L;

	/** @generated */
	public Order() {
	}

	/**
	 * ------------------------------------------
	 * The primary key.
	 *
	 * @generated
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	/** @generated */
	public void setId(final Long id) {
		this.id = id;
	}

	/** @generated */
	private Long id = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@Column(length = 0, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public Date getOrderDate() {	
		return orderDate;
	}

	/** @generated */
	public void setOrderDate(final Date orderDate) {
		this.orderDate = orderDate;
	}

	/** @generated */
	private Date orderDate = new Date();

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@Column(length = 0, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public Long getTotal() {
		return total;
	}

	/** @generated */
	public void setTotal(final Long total) {
		this.total = total;
	}

	/** @generated */
	private Long total = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@Column(length = 0, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public Double getPrice() {
		return price;
	}

	/** @generated */
	public void setPrice(final Double price) {
		this.price = price;
	}

	/** @generated */
	private Double price = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	public Bottle getBottle() {
		return bottle;
	}

	/** @generated */
	public void setBottle(final Bottle bottle) {
		this.bottle = bottle;
	}

	/** @generated */
	private Bottle bottle = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	public Basket getBasket() {
		return basket;
	}

	/** @generated */
	public void setBasket(final Basket basket) {
		this.basket = basket;
	}

	/** @generated */
	private Basket basket = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "order", cascade = CascadeType.REFRESH)
	public List<OrderDetail> getOrderDetails() {
		if (this.orderDetails == null) {
			this.orderDetails = new ArrayList<OrderDetail>();
		}
		return orderDetails;
	}

	/** @generated */
	public void setOrderDetails(final List<OrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}

	/**
	 * Associate Order with OrderDetail
	 * @generated
	 */
	public void addOrderDetail(OrderDetail orderDetail) {
		if (orderDetail == null) {
			return;
		}
		getOrderDetails().add(orderDetail);
		orderDetail.setOrder(this);
	}

	/**
	 * Unassociate Order from OrderDetail
	 * @generated
	 */
	public void removeOrderDetail(OrderDetail orderDetail) {
		if (orderDetail == null) {
			return;
		}
		getOrderDetails().remove(orderDetail);
		orderDetail.setOrder(null);
	}

	/**
	 * @generated
	 */
	public void removeAllOrderDetails() {
		List<OrderDetail> remove = new ArrayList<OrderDetail>();
		remove.addAll(getOrderDetails());
		for (OrderDetail element : remove) {
			removeOrderDetail(element);
		}
	}

	/** @generated */
	private List<OrderDetail> orderDetails = null;

	/**
	 * ------------------------------------------
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	public DeliveryPoint getDeliveryPoint() {
		return deliveryPoint;
	}

	/** @generated */
	public void setDeliveryPoint(final DeliveryPoint deliveryPoint) {
		this.deliveryPoint = deliveryPoint;
	}

	/** @generated */
	private DeliveryPoint deliveryPoint = null;

	// ------------------------------------------
	// Utils
	// ------------------------------------------



	/** @generated */
	public Order deepClone() throws Exception {
		Order clone = (Order) super.clone();
		clone.setId(null);

		clone.setOrderDetails(null);
		for (OrderDetail kid : this.getOrderDetails()) {
			clone.addOrderDetail(kid.deepClone());
		}
		return clone;
	}
	
	/** @generated */
	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ((id == null) ? super.hashCode() : id.hashCode());
		return result;
	}

	/** @generated */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Order))
			return false;
		final Order other = (Order) obj;
		if (id == null) {
			if (other.getId() != null)
				return false;
		} else if (!id.equals(other.getId()))
			return false;
		return true;
	}	
}
