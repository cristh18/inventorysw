package com.nsdlp.co.model;

import com.nsdlp.co.model.Inventory;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

/**
 *
 * @author Cristhian Leonardo Tolosa
 */
@Entity
@DiscriminatorValue(value = "ITEM_BASKET")
public class Basket extends Inventory {
	/** @generated */
	private static final long serialVersionUID = 1L;

	/** @generated */
	public Basket() {
	}

	/**
	 * ------------------------------------------
	 * 
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "basket", cascade = CascadeType.REFRESH)
	public List<Order> getOrders() {
		if (this.orders == null) {
			this.orders = new ArrayList<Order>();
		}
		return orders;
	}

	/** @generated */
	public void setOrders(final List<Order> orders) {
		this.orders = orders;
	}

	/**
	 * Associate Basket with Order
	 * 
	 * @generated
	 */
	public void addOrder(Order order) {
		if (order == null) {
			return;
		}
		getOrders().add(order);
		order.setBasket(this);
	}

	/**
	 * Unassociate Basket from Order
	 * 
	 * @generated
	 */
	public void removeOrder(Order order) {
		if (order == null) {
			return;
		}
		getOrders().remove(order);
		order.setBasket(null);
	}

	/**
	 * @generated
	 */
	public void removeAllOrders() {
		List<Order> remove = new ArrayList<Order>();
		remove.addAll(getOrders());
		for (Order element : remove) {
			removeOrder(element);
		}
	}

	/** @generated */
	private List<Order> orders = null;

	/**
	 * ------------------------------------------
	 * 
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "basket", cascade = CascadeType.REFRESH)
	public List<Reception> getReceptions() {
		if (this.receptions == null) {
			this.receptions = new ArrayList<Reception>();
		}
		return receptions;
	}

	/** @generated */
	public void setReceptions(final List<Reception> receptions) {
		this.receptions = receptions;
	}

	/**
	 * Associate Basket with Reception
	 * 
	 * @generated
	 */
	public void addReception(Reception reception) {
		if (reception == null) {
			return;
		}
		getReceptions().add(reception);
		reception.setBasket(this);
	}

	/**
	 * Unassociate Basket from Reception
	 * 
	 * @generated
	 */
	public void removeReception(Reception reception) {
		if (reception == null) {
			return;
		}
		getReceptions().remove(reception);
		reception.setBasket(null);
	}

	/**
	 * @generated
	 */
	public void removeAllReceptions() {
		List<Reception> remove = new ArrayList<Reception>();
		remove.addAll(getReceptions());
		for (Reception element : remove) {
			removeReception(element);
		}
	}

	/** @generated */
	private List<Reception> receptions = null;

	// ------------------------------------------
	// Utils
	// ------------------------------------------

	/** @generated */
	public Basket deepClone() throws Exception {
		Basket clone = (Basket) super.deepClone();

		clone.setOrders(null);
		for (Order kid : this.getOrders()) {
			clone.addOrder(kid.deepClone());
		}

		clone.setReceptions(null);
		for (Reception kid : this.getReceptions()) {
			clone.addReception(kid.deepClone());
		}
		return clone;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return new StringBuilder().append("Basket [ID=").append(super.getId())
				.append("][NAM=").append(super.getName()).append("]")
				.toString();
	}
}
