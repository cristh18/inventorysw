package com.nsdlp.co.service;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.nsdlp.co.model.Basket;

@Stateless
public class BasketRegistration {
	
	@Inject
	private Logger log;

	@Inject
	private EntityManager em;

	@Inject
	private Event<Basket> basketEventSrc;

	/**
	 * Register basket
	 * @param basket
	 * @throws Exception
	 */
	public void register(Basket basket) throws Exception {
		log.info("Registering " + basket.getName());
		em.persist(basket);
		basketEventSrc.fire(basket);
	}
	
	/**
	 * Update basket data
	 * @param basket
	 * @return
	 */
	public Basket update(Basket basket){
		log.info("Updating " + basket.getName());
		
		em.merge(basket);
		basketEventSrc.fire(basket);
		return basket;
	}


}
