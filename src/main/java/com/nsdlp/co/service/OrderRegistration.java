package com.nsdlp.co.service;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.nsdlp.co.model.Order;

@Stateless
public class OrderRegistration {

	@Inject
	private Logger log;

	@Inject
	private EntityManager em;

	@Inject
	private Event<Order> orderEventSrc;

	public void register(Order order) throws Exception {
		log.info("Registering " + order.getOrderDate().toString());
		em.persist(order);
		orderEventSrc.fire(order);
	}

}
