package com.nsdlp.co.service;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.nsdlp.co.model.Client;

@Stateless
public class ClientRegistration {
	@Inject
	private Logger log;

	@Inject
	private EntityManager em;

	@Inject
	private Event<Client> clientEventSrc;

	public void register(Client client) throws Exception {
		log.info("Registering " + client.getName());
		em.persist(client);
		clientEventSrc.fire(client);
	}
}
