package com.nsdlp.co.service;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.nsdlp.co.model.Reception;

@Stateless
public class ReceptionRegistration {
	
	@Inject
	private Logger log;

	@Inject
	private EntityManager em;

	@Inject
	private Event<Reception> receptionEventSrc;

	public void register(Reception reception) throws Exception {
		log.info("Registering " + reception.getReceptionDate().toString());
		em.persist(reception);
		receptionEventSrc.fire(reception);
	}

}
