package com.nsdlp.co.service;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.nsdlp.co.model.Bottle;

@Stateless
public class BottleRegistration {
	
	@Inject
	private Logger log;

	@Inject
	private EntityManager em;

	@Inject
	private Event<Bottle> bottleEventSrc;

	/**
	 * Register bottle
	 * @param bottle
	 * @throws Exception
	 */
	public void register(Bottle bottle) throws Exception {
		log.info("Registering " + bottle.getName());
		em.persist(bottle);
		bottleEventSrc.fire(bottle);
	}
	
	/**
	 * Update bottle data
	 * @param bottle
	 * @return
	 */
	public Bottle update(Bottle bottle){
		log.info("Updating " + bottle.getName());
		
		em.merge(bottle);
		bottleEventSrc.fire(bottle);
		return bottle;
	}

}
