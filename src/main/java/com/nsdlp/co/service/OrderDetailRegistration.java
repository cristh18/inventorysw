package com.nsdlp.co.service;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.nsdlp.co.model.OrderDetail;

@Stateless
public class OrderDetailRegistration {

	@Inject
	private Logger log;

	@Inject
	private EntityManager em;

	@Inject
	private Event<OrderDetail> orderDetailEventSrc;

	public void register(OrderDetail orderDetail) throws Exception {
		log.info("Registering " + orderDetail.getOrderDateDetail().toString());
		em.persist(orderDetail);
		orderDetailEventSrc.fire(orderDetail);
	}

}
