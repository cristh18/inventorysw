package com.nsdlp.co.controller;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.nsdlp.co.model.Client;

@Model
public class ClientController {

	@Inject
	private FacesContext facesContext;
	
	@Produces
	@Named
	private Client client;

	// Contructors
	@PostConstruct
	public void initNewClient() {

	}
	
	//Methods
	
	
	 //geters and setters
	
	/**
	 * 
	 * @return
	 */
	public Client getClient() {
		return client;
	}

}
