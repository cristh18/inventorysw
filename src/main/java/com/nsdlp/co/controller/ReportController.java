package com.nsdlp.co.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.nsdlp.co.constants.InventoryConstants;
import com.nsdlp.co.data.DeliveryPointRepository;
import com.nsdlp.co.data.OrderDetailRepository;
import com.nsdlp.co.data.OrderRepository;
import com.nsdlp.co.model.Client;
import com.nsdlp.co.model.DeliveryPoint;
import com.nsdlp.co.model.Order;
import com.nsdlp.co.model.OrderDetail;

@Model
@ViewScoped
public class ReportController {

	@Inject
	private FacesContext facesContext;

	@Inject
	private OrderRepository orderRepository;

	@Inject
	private DeliveryPointRepository deliveryPointRepository;
	
	@Inject
	private OrderDetailRepository orderDetailRepository;

	@Produces
	@Named
	private List<Order> searchOrderList;

	@Produces
	@Named
	private Client searchSelectedClient;

	@Produces
	@Named
	private DeliveryPoint searchSelectedDeliveryPoint;

	@Produces
	@Named
	private Date searchStartDate;

	@Produces
	@Named
	private Date searchFinalDate;

	@Produces
	@Named
	private static List<DeliveryPoint> deliveryPointsByClientReport;
	
	@Produces
    @Named
    private Order newReportOrder;
	
	/**
	 * 
	 */
	private boolean disableDeliveryPointReport;
	
	/**
	 * 
	 */
	@Produces
	@Named
	private List<OrderDetail> orderDetailList;

	// Contructors
	@PostConstruct
	private void initNewOrder() {		
		newReportOrder = new Order();
		
		updateDataComboDeliveryPointReport();
		
		orderDetailList = new ArrayList<OrderDetail>();
		
		getAllOrders();
	}

	/**
	 * 
	 */
	public void getAllOrders() {		
		try {
			searchOrderList = orderRepository.findAllOrderedByName();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
     * 
     */
	public void searchOrders() {
		searchOrderList = orderRepository.findOrdersByFilters(searchStartDate,
				searchFinalDate, searchSelectedClient, searchSelectedDeliveryPoint);
		if (searchOrderList == null) {
			getAllOrders();
		}
	}

	/**
	 * Update delivery Point by client
	 */
	public void updateDataComboDeliveryPointReport() {
		if (searchSelectedClient != null) {	
			deliveryPointsByClientReport = deliveryPointRepository
					.findAllOrderedByNameByClientId(searchSelectedClient);
			setDisableDeliveryPointReport(false);
			
		}else {
			setDisableDeliveryPointReport(true);
		}
	}

	/**
	 * 
	 * @return
	 */
	public void loadOrderDetailList(Order order) {
		System.out.println("========================================");
		orderDetailList = orderDetailRepository.findAllOrderDetailsByOrder(order);
//		orderDetailList = orderDetailRepository.findAllOrderedByName();
		for (OrderDetail orderDetail : orderDetailList) {
			if (orderDetail.getItemName().equalsIgnoreCase(InventoryConstants.ITEM_NAME_BOTTLE)) {
				orderDetail.setPrice(orderDetail.getTotal().doubleValue()*InventoryConstants.PRICE_BOTTLE);
			}else if (orderDetail.getItemName().equalsIgnoreCase(InventoryConstants.ITEM_NAME_BASKET)) {
				orderDetail.setPrice(orderDetail.getTotal().doubleValue()*InventoryConstants.PRICE_BASKET);
			}
			
			System.out.println(orderDetail.getId());
			System.out.println(orderDetail.getOrder().getId());
			System.out.println(orderDetail.getItemId());
			System.out.println(orderDetail.getItemName());
			System.out.println(orderDetail.getPrice());
			
			System.out.println("========================================");
		}
	}
	
	
	// Getters and setters

	/**
	 * 
	 * @return
	 */
	public List<Order> getSearchOrderList() {
		return searchOrderList;
	}

	/**
	 * 
	 * @return
	 */
	public Client getSearchSelectedClient() {
		return searchSelectedClient;
	}

	/**
	 * 
	 * @param searchSelectedClient
	 */
	public void setSearchSelectedClient(Client searchSelectedClient) {
		this.searchSelectedClient = searchSelectedClient;
	}

	/**
	 * 
	 * @return
	 */
	public Date getSearchStartDate() {
		return searchStartDate;
	}

	/**
	 * 
	 * @param searchStartDate
	 */
	public void setSearchStartDate(Date searchStartDate) {
		this.searchStartDate = searchStartDate;
	}

	/**
	 * 
	 * @return
	 */
	public Date getSearchFinalDate() {
		return searchFinalDate;
	}

	/**
	 * 
	 * @param searchFinalDate
	 */
	public void setSearchFinalDate(Date searchFinalDate) {
		this.searchFinalDate = searchFinalDate;
	}

	/**
	 * 
	 * @return
	 */
	public static List<DeliveryPoint> getDeliveryPointsByClientReport() {
		return deliveryPointsByClientReport;
	}
	
	/**
	 * 
	 * @param deliveryPointsByClientReport
	 */
	public static void setDeliveryPointsByClientReport(
			List<DeliveryPoint> deliveryPointsByClientReport) {
		ReportController.deliveryPointsByClientReport = deliveryPointsByClientReport;
	}

	/**
	 * 
	 * @return
	 */
	public Order getNewReportOrder() {
		return newReportOrder;
	}
	
	/**
	 * 
	 * @param newReportOrder
	 */
	public void setNewReportOrder(Order newReportOrder) {
		this.newReportOrder = newReportOrder;
	}

	/**
	 * 
	 * @return
	 */
	public DeliveryPoint getSearchSelectedDeliveryPoint() {
		return searchSelectedDeliveryPoint;
	}
	
	/**
	 * 
	 * @param searchSelectedDeliveryPoint
	 */
	public void setSearchSelectedDeliveryPoint(
			DeliveryPoint searchSelectedDeliveryPoint) {
		this.searchSelectedDeliveryPoint = searchSelectedDeliveryPoint;
	}
	
	/**
	 * @return the disableDeliveryPointReport
	 */
	public boolean isDisableDeliveryPointReport() {
		return disableDeliveryPointReport;
	}

	/**
	 * @param disableDeliveryPointReport the disableDeliveryPointReport to set
	 */
	public void setDisableDeliveryPointReport(boolean disableDeliveryPointReport) {
		this.disableDeliveryPointReport = disableDeliveryPointReport;
	}

	/**
	 * @return the orderDetailList
	 */
	public List<OrderDetail> getOrderDetailList() {
		return orderDetailList;
	}

	/**
	 * @param orderDetailList the orderDetailList to set
	 */
	public void setOrderDetailList(List<OrderDetail> orderDetailList) {
		this.orderDetailList = orderDetailList;
	}
}
