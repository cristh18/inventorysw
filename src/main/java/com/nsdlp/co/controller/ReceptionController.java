package com.nsdlp.co.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.nsdlp.co.data.BasketRepository;
import com.nsdlp.co.data.BottleRepository;
import com.nsdlp.co.data.DeliveryPointRepository;
import com.nsdlp.co.model.Basket;
import com.nsdlp.co.model.Bottle;
import com.nsdlp.co.model.Client;
import com.nsdlp.co.model.DeliveryPoint;
import com.nsdlp.co.model.Reception;
import com.nsdlp.co.service.BasketRegistration;
import com.nsdlp.co.service.BottleRegistration;
import com.nsdlp.co.service.ReceptionRegistration;

@Model
public class ReceptionController {
	
	@Inject
    private FacesContext facesContext;

    @Inject
    private ReceptionRegistration receptionRegistration;
    
    @Inject
    private BottleRegistration bottleRegistration;
    
    @Inject
    private BasketRegistration basketRegistration;
    
    @Inject
    private BottleRepository bottleRepository;
    
    @Inject
    private BasketRepository basketRepository;
    
    @Inject
    private DeliveryPointRepository deliveryPointRepository;

    @Produces
    @Named
    private Reception reception;
    
    @Produces
    @Named
    private Bottle bottleRx;
    
    @Produces
    @Named
    private Basket basketRx;
    
    @Produces
    @Named
    private Client selectedClientRx;
    
    @Produces
    @Named
    private DeliveryPoint selectedDeliveryPointRx;
    
    @Produces
    @Named
    private List<DeliveryPoint> deliveryPointsByClientRx;
    
    private boolean enableDeliveryPoint;
    
 // Contructors
    @PostConstruct
    public void initNewOrder() {
    	reception = new Reception();
        bottleRx = new Bottle();
        basketRx = new Basket();
    }
    
    //Methods
    
       
    /**
     * Update inventory from order
     */
    public void updateInventory() {
    	Bottle editedBottle = bottleRepository.findById(1L);
    	Basket editedBasket = basketRepository.findById(2L);
    	
    	long quantityBottleTemp = editedBottle.getQuantity()+bottleRx.getQuantity();
    	long quantityBasketTemp = editedBasket.getQuantity()+basketRx.getQuantity();
    	
    	editedBottle.setQuantity(quantityBottleTemp);
    	editedBasket.setQuantity(quantityBasketTemp);
    	
		bottleRegistration.update(editedBottle);
		basketRegistration.update(editedBasket);
		initNewOrder();
	}
    
    /**
     * Order Register
     * @throws Exception
     */
    public void register() throws Exception {
    	
        try {
        	        	
        	System.out.println("BOTTLE: " + bottleRepository.findBottleByName().getId() + "ITEM_NAME: " + bottleRepository.findBottleByName().getName());
        	
        	if (bottleRx.getQuantity() == null) {
				bottleRx.setQuantity(0L);
			}
        	
        	if (basketRx.getQuantity() == null) {
				basketRx.setQuantity(0L);
			}
        	
        	
        	reception.setTotal(bottleRx.getQuantity()+basketRx.getQuantity());        	 
        	reception.setBottle(bottleRepository.findBottleByName());
        	reception.setBasket(basketRepository.findBasketByName());        	
            receptionRegistration.register(reception);
            FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registered!", "Registration successful");
            facesContext.addMessage(null, m);
//            initNewOrder();
        } catch (Exception e) {
            String errorMessage = getRootErrorMessage(e);
            FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, "Registration unsuccessful");
            facesContext.addMessage(null, m);
        }
    }
    
	/**
	 * Event datapicker
	 * 
	 * @throws Exception
	 */
	public void click() throws Exception {
		RequestContext requestContext = RequestContext.getCurrentInstance();

		requestContext.update("form:display");
		requestContext.execute("PF('dlg').show()");

		register();
		updateInventory();

	}
	
    
    /**
     * 
     * @param e
     * @return
     */
    private String getRootErrorMessage(Exception e) {
        // Default to general error message that registration failed.
        String errorMessage = "Registration failed. See server log for more information";
        if (e == null) {
            // This shouldn't happen, but return the default messages
            return errorMessage;
        }

        // Start with the exception and recurse to find the root cause
        Throwable t = e;
        while (t != null) {
            // Get the message from the Throwable class instance
            errorMessage = t.getLocalizedMessage();
            t = t.getCause();
        }
        // This is the root cause message
        return errorMessage;
    }

	/**
	 * @return the reception
	 */
	public Reception getReception() {
		return reception;
	}

	/**
	 * @param reception the reception to set
	 */
	public void setReception(Reception reception) {
		this.reception = reception;
	}

	/**
	 * @return the bottleRx
	 */
	public Bottle getBottleRx() {
		return bottleRx;
	}

	/**
	 * @param bottleRx the bottleRx to set
	 */
	public void setBottleRx(Bottle bottleRx) {
		this.bottleRx = bottleRx;
	}

	/**
	 * @return the basketRx
	 */
	public Basket getBasketRx() {
		return basketRx;
	}

	/**
	 * @param basketRx the basketRx to set
	 */
	public void setBasketRx(Basket basketRx) {
		this.basketRx = basketRx;
	}

	/**
	 * @return the selectedClientRx
	 */
	public Client getSelectedClientRx() {
		return selectedClientRx;
	}

	/**
	 * @param selectedClientRx the selectedClientRx to set
	 */
	public void setSelectedClientRx(Client selectedClientRx) {
		this.selectedClientRx = selectedClientRx;
	}

	/**
	 * @return the deliveryPointsByClientRx
	 */
	public List<DeliveryPoint> getDeliveryPointsByClientRx() {
		return deliveryPointsByClientRx;
	}

	/**
	 * @param deliveryPointsByClientRx the deliveryPointsByClientRx to set
	 */
	public void setDeliveryPointsByClientRx(
			List<DeliveryPoint> deliveryPointsByClientRx) {
		this.deliveryPointsByClientRx = deliveryPointsByClientRx;
	}

	/**
	 * @return the enableDeliveryPoint
	 */
	public boolean isEnableDeliveryPoint() {
		return enableDeliveryPoint;
	}

	/**
	 * @param enableDeliveryPoint the enableDeliveryPoint to set
	 */
	public void setEnableDeliveryPoint(boolean enableDeliveryPoint) {
		this.enableDeliveryPoint = enableDeliveryPoint;
	}
    
    //geters and setters
    
    
}
