package com.nsdlp.co.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.nsdlp.co.constants.InventoryConstants;
import com.nsdlp.co.data.BasketRepository;
import com.nsdlp.co.data.BottleRepository;
import com.nsdlp.co.data.DeliveryPointRepository;
import com.nsdlp.co.model.Basket;
import com.nsdlp.co.model.Bottle;
import com.nsdlp.co.model.Client;
import com.nsdlp.co.model.DeliveryPoint;
import com.nsdlp.co.model.Order;
import com.nsdlp.co.model.OrderDetail;
import com.nsdlp.co.service.BasketRegistration;
import com.nsdlp.co.service.BottleRegistration;
import com.nsdlp.co.service.OrderDetailRegistration;
import com.nsdlp.co.service.OrderRegistration;

@Model
@ViewScoped
public class OrderController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private FacesContext facesContext;

	@Inject
	private OrderRegistration orderRegistration;
	
	@Inject
	private OrderDetailRegistration orderDetailRegistration;

	@Inject
	private BottleRegistration bottleRegistration;

	@Inject
	private BasketRegistration basketRegistration;

	@Inject
	private BottleRepository bottleRepository;

	@Inject
	private BasketRepository basketRepository;

	@Inject
	private DeliveryPointRepository deliveryPointRepository;

	@Produces
	@Named
	private Order newOrder;
	
	@Produces
	@Named
	private Bottle bottle;

	@Produces
	@Named
	private Basket basket;

	@Produces
	@Named
	private Client selectedClient;

	@Produces
	@Named
	private DeliveryPoint selectedDeliveryPoint;

	@Produces
	@Named
	private static List<DeliveryPoint> deliveryPointsByClient;

	private boolean enableDeliveryPoint;

	private static int tempBottles;

	private static int tempBaskets;

	private int tempTotal;

	private double tempPrice;

	// Contructors
	@PostConstruct
	public void initNewOrder() {
		newOrder = new Order();
		bottle = new Bottle();
		basket = new Basket();

		updateDataComboDeliveryPoint();
		// selectedClient = new Client();
		// selectedDeliveryPoint = new DeliveryPoint();
		// setEnableDeliveryPoint(true);
	}

	// Methods

	/**
	 * Calculate total price from order
	 * 
	 * @return
	 */
	private int calculateOrderPrice() {
		int priceBottle = InventoryConstants.PRICE_BOTTLE;
		int priceBasket = InventoryConstants.PRICE_BASKET;
		int totalPriceBottle = (int) (priceBottle * bottle.getQuantity());
		int totalPriceBasket = (int) (priceBasket * basket.getQuantity());
		return totalPriceBottle + totalPriceBasket;
	}

	/**
	 * Update inventory from order
	 */
	public void updateInventory() {
		Bottle editedBottle = bottleRepository.findById(1L);
		Basket editedBasket = basketRepository.findById(2L);

		long quantityBottleTemp = editedBottle.getQuantity()
				- bottle.getQuantity();
		long quantityBasketTemp = editedBasket.getQuantity()
				- basket.getQuantity();

		editedBottle.setQuantity(quantityBottleTemp);
		editedBasket.setQuantity(quantityBasketTemp);

		bottleRegistration.update(editedBottle);
		basketRegistration.update(editedBasket);
		initNewOrder();
	}

	/**
	 * Order Register
	 * 
	 * @throws Exception
	 */
	public void register() throws Exception {
		double orderPrice = 0.0;
		try {
			System.out.println(this.calculateOrderPrice());

			System.out.println("BOTTLE: "
					+ bottleRepository.findBottleByName().getId()
					+ "ITEM_NAME: "
					+ bottleRepository.findBottleByName().getName());

			if (bottle.getQuantity() == null) {
				bottle.setQuantity(0L);
			}

			if (basket.getQuantity() == null) {
				basket.setQuantity(0L);
			}

			orderPrice = this.calculateOrderPrice();
			newOrder.setTotal(bottle.getQuantity() + basket.getQuantity());
			newOrder.setPrice(orderPrice);
			newOrder.setBottle(bottleRepository.findBottleByName());
			newOrder.setBasket(basketRepository.findBasketByName());
			orderRegistration.register(newOrder);
			
			List<OrderDetail> orderDetails = new ArrayList<OrderDetail>();
			
			OrderDetail orderDetailBottle = new OrderDetail();
			OrderDetail orderDetailBasket = new OrderDetail();
			
			if (bottle.getQuantity() != null && bottle.getQuantity() != 0) {				
				orderDetailBottle.setItemId(bottleRepository.findBottleByName().getId());
				orderDetailBottle.setItemName(bottleRepository.findBottleByName().getName());
				orderDetailBottle.setOrder(newOrder);
				orderDetailBottle.setOrderDateDetail(newOrder.getOrderDate());
				orderDetailBottle.setTotal(bottle.getQuantity());
				orderDetails.add(orderDetailBottle);
			}
			
			if (basket.getQuantity() != null && basket.getQuantity() != 0) {
				orderDetailBasket.setItemId(basketRepository.findBasketByName().getId());
				orderDetailBasket.setItemName(basketRepository.findBasketByName().getName());
				orderDetailBasket.setOrder(newOrder);
				orderDetailBasket.setOrderDateDetail(newOrder.getOrderDate());
				orderDetailBasket.setTotal(basket.getQuantity());
				orderDetails.add(orderDetailBasket);
			}
			
			if (!orderDetails.isEmpty()) {
				for (OrderDetail orderDetail : orderDetails) {
					orderDetailRegistration.register(orderDetail);
				}
			}
						
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO,
					"Registrado", "Registro exitoso");
			facesContext.addMessage(null, m);
		} catch (Exception e) {
			String errorMessage = getRootErrorMessage(e);
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					errorMessage, "Registration unsuccessful");
			facesContext.addMessage(null, m);
		}
	}

	/**
	 * Event datapicker
	 * 
	 * @throws Exception
	 */
	public void click() throws Exception {
		RequestContext requestContext = RequestContext.getCurrentInstance();

		requestContext.update("form:display");
		requestContext.execute("PF('dlg').show()");

		register();
		updateInventory();

	}

	/**
	 * Update delivery Point by client
	 */
	public void updateDataComboDeliveryPoint() {
		if (selectedClient != null) {
			deliveryPointsByClient = deliveryPointRepository
					.findAllOrderedByNameByClientId(selectedClient);
			setEnableDeliveryPoint(false);
		} else {
			setEnableDeliveryPoint(true);
		}

	}

	/**
	 * Update Total and price fields in view
	 */
	public void updateTotalAndPriceFields() {
		tempTotal = calculateTempBottles() + calculateTempBaskets();
		tempPrice = (tempBottles * InventoryConstants.PRICE_BOTTLE)
				+ (tempBaskets * InventoryConstants.PRICE_BASKET);
	}

	/**
	 * Calculate the bottle number from order
	 * 
	 * @return tempBottles Bottles Number
	 */
	public int calculateTempBottles() {

		if (bottle.getQuantity() != null) {
			this.setTempBottles(bottle.getQuantity().intValue());
		}

		return tempBottles;
	}

	/**
	 * Calculate the basket number from order
	 * 
	 * @return tempBaskets Baskets Number
	 */
	public int calculateTempBaskets() {

		if (basket.getQuantity() != null) {
			this.setTempBaskets(basket.getQuantity().intValue());
		}
		return tempBaskets;
	}

	/**
	 * 
	 * @param e
	 * @return
	 */
	private String getRootErrorMessage(Exception e) {
		// Default to general error message that registration failed.
		String errorMessage = "Registration failed. See server log for more information";
		if (e == null) {
			// This shouldn't happen, but return the default messages
			return errorMessage;
		}

		// Start with the exception and recurse to find the root cause
		Throwable t = e;
		while (t != null) {
			// Get the message from the Throwable class instance
			errorMessage = t.getLocalizedMessage();
			t = t.getCause();
		}
		// This is the root cause message
		return errorMessage;
	}

	// geters and setters

	/**
	 * 
	 * @return
	 */
	public Order getNewOrder() {
		return newOrder;
	}
	
	/**
	 * 
	 * @return
	 */
	public Bottle getBottle() {
		return bottle;
	}

	/**
	 * 
	 * @return
	 */
	public Basket getBasket() {
		return basket;
	}

	/**
	 * 
	 * @return
	 */
	public Client getSelectedClient() {
		return selectedClient;
	}

	/**
	 * 
	 * @param selectedClient
	 */
	public void setSelectedClient(Client selectedClient) {
		this.selectedClient = selectedClient;
	}

	/**
	 * 
	 * @return
	 */
	public DeliveryPoint getSelectedDeliveryPoint() {
		return selectedDeliveryPoint;
	}

	/**
	 * 
	 * @param selectedDeliveryPoint
	 */
	public void setSelectedDeliveryPoint(DeliveryPoint selectedDeliveryPoint) {
		this.selectedDeliveryPoint = selectedDeliveryPoint;
	}

	/**
	 * 
	 * @return
	 */
	public static List<DeliveryPoint> getDeliveryPointsByClient() {
		return deliveryPointsByClient;
	}

	/**
	 * 
	 * @param deliveryPointsByClient
	 */
	public static void setDeliveryPointsByClient(
			List<DeliveryPoint> deliveryPointsByClient) {
		OrderController.deliveryPointsByClient = deliveryPointsByClient;
	}

	/**
	 * @return the enableDeliveryPoint
	 */
	public boolean isEnableDeliveryPoint() {
		return enableDeliveryPoint;
	}

	/**
	 * @param enableDeliveryPoint
	 *            the enableDeliveryPoint to set
	 */
	public void setEnableDeliveryPoint(boolean enableDeliveryPoint) {
		this.enableDeliveryPoint = enableDeliveryPoint;
	}

	public int getTempTotal() {
		return tempTotal;
	}

	public void setTempTotal(int tempTotal) {
		this.tempTotal = tempTotal;
	}

	public static int getTempBottles() {
		return tempBottles;
	}

	public static void setTempBottles(int tempBottles) {
		OrderController.tempBottles = tempBottles;
	}

	public static int getTempBaskets() {
		return tempBaskets;
	}

	public static void setTempBaskets(int tempBaskets) {
		OrderController.tempBaskets = tempBaskets;
	}

	public double getTempPrice() {
		return tempPrice;
	}

	public void setTempPrice(double tempPrice) {
		this.tempPrice = tempPrice;
	}

}
