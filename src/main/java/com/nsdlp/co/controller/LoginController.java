package com.nsdlp.co.controller;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.nsdlp.co.data.UserRepository;
import com.nsdlp.co.model.User;

@Model
public class LoginController {

	@Inject
	private FacesContext facesContext;

	@Inject
	private UserRepository userRepository;

	@Produces
	@Named
	private User newUser;

	@PostConstruct
	public void initnewUser() {
		newUser = new User();
	}

	/**
	 * Validate access 
	 * @return
	 */
	public String login() {
		String pageForward = "";
		User userTemp = userRepository.findUser(newUser.getUserName(),
				newUser.getUserPassword());
		if (userTemp != null) {
			pageForward = "home?faces-redirect=true";
		} else {
			pageForward = null;
		}

		return pageForward;
	}

	/**
	 * 
	 * @return
	 */
	public User getNewUser() {
		return newUser;
	}

}
