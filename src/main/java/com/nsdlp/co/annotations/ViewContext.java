/**
 * ViewContext.java
 */
package com.nsdlp.co.annotations;
import javax.enterprise.context.spi.Context;
import javax.enterprise.context.spi.Contextual;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import java.lang.annotation.Annotation;
import java.util.Map;

/**
 * <b>Descripcion: </b>Define el contexto para manejar el scoped de view
 * @author 
 */
public class ViewContext implements Context {

    @Override
    public Class<? extends Annotation> getScope() {
        return ViewScoped.class;
    }

	@SuppressWarnings("unchecked")
	@Override
    public <T> T get(Contextual<T> contextual, CreationalContext<T> creationalContext) {
        Bean<T> bean = (Bean<T>) contextual;
        @SuppressWarnings("rawtypes")
		Map viewMap = getViewMap();
        if (viewMap.containsKey(bean.getName())) {
            return (T) viewMap.get(bean.getName());
        } else {
            T t = bean.create(creationalContext);
            viewMap.put(bean.getName(), t);
            return t;
        }

    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
    public <T> T get(Contextual<T> contextual) {
        Bean bean = (Bean) contextual;
        Map viewMap = getViewMap();
        if (viewMap.containsKey(bean.getName())) {
            return (T) viewMap.get(bean.getName());
        } else {
            return null;
        }
    }

    @SuppressWarnings("rawtypes")
	private Map getViewMap() {
        FacesContext fctx = FacesContext.getCurrentInstance();
        UIViewRoot viewRoot = fctx.getViewRoot();
        return viewRoot.getViewMap(true);
    }


    public boolean isActive() {
        return true;
    }
}