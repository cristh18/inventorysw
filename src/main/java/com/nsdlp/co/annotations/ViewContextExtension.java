/**
 * ViewContextExtension.java
 */
package com.nsdlp.co.annotations;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.AfterBeanDiscovery;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.Extension;

/**
 * <b>Descripcion: </b>Define la extension para el manejo de la anotacion de viewscoped
 * @author 
 */
public class ViewContextExtension implements Extension{

    public void afterBeanDiscovery(@Observes AfterBeanDiscovery event, BeanManager manager) {
        event.addContext(new ViewContext());
    }
}
